<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function ($router) {
    // User Group Routes
	$router->group(['prefix' => 'users'], function ($router) {
		
		$router->post('login/','UsersController@authenticate');
		$router->post('create/', 'UsersController@add');
		$router->get('view/{id}', 'UsersController@view');
		$router->get('logout', 'UsersController@logout');
		
	});
	
	// Blog Group Routes
	$router->group(['prefix' => 'blog'], function ($router) {
		
		$router->get('/', 'BlogsController@index');
		$router->get('/{id}/', 'BlogsController@show');
		$router->post('/','BlogsController@store');
		$router->put('/{id}/', 'BlogsController@update');
		$router->delete('/{id}/', 'BlogsController@destroy');
		
	});
	
	// Media Group Routes
	$router->group(['prefix' => 'media'], function ($router) {
		
		$router->post('/','MediasController@store');
		
	});
});