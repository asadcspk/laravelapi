<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $table = 'api_blogs';
	
	protected $fillable = [ 'title', 'description', 'user_id' ];
	
	protected $hidden = [
       'user_id'
    ];
	
}