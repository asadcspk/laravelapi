<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;
use Auth;

class MediasController extends Controller
{
    public function __construct(){
		$this->middleware('auth');	
	}
	
    public function index(Request $request){
		//$blog = Auth::user()->blog()->get();
        //return response()->json(['status' => 'success','result' => $blog]);
	}
	
	public function store(Request $request){
		//echo "<pre>";print_r($request->all());exit;
		$this->validate($request, [
		  'name' => 'required',
		  'blog_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
		]);
		
		$image_url = '';
		if( $request->hasFile('blog_image') && $request->file('blog_image')->isValid() ){
			$year = date('Y');
			$month = date('m');
			$year_dir = 'images/'.$year;
			$month_dir = 'images/'.$year.'/'.$month;
			$image_dir = base_path('public').'/'.$month_dir;
			
			if( !file_exists($image_dir) ){
				mkdir($image_dir, 0777, true);
			}
			
			$image = $request->file('blog_image');
			$name = str_slug($request->name).'.'.$image->getClientOriginalExtension();
			if( $image->move($image_dir, $name) ){
				$image_url = url('/public').'/'.$month_dir.'/'.$name;
				$request->request->add(['url' => $image_url]);
			}
			
		}
		//echo "<pre>";print_r($request->all());exit;
		
		if( $media = Auth::user()->media()->create($request->all()) ){
			$lastInsertedId = $media->id;
			$media = Auth::user()->media()->where('id', $lastInsertedId)->get();
			return response()->json($media, 200);
		}else{
            return response()->json(['status' => 'fail']);
        }
	}
	
}
