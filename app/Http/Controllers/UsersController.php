<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Hash;
use Auth;

class UsersController extends Controller
{
    public function __construct(){
		$this->middleware('auth:api', ['except' => ['add', 'authenticate']]);
	}
	
    public function add(Request $request){
		/* if( $request->user()->role != 'admin' ){
			return response()->json(['error' => 'Only authorize person can create new user'], 401);
		} */
		$this->validate($request, [
			'email'	=> 'required|email|unique:api_users',
			'password'	=> 'required',
			'role'	=> 'required|in:admin,user',
		]);
		$request['password'] = app('hash')->make($request['password']); // Same as bcrypt($request['password']);
		$user = User::create($request->all());
		return response()->json($user); 
	}
	
	public function authenticate(Request $request){
		$this->validate($request, [
			'email'	=> 'required',
			'password'	=> 'required',
		]);
		
		$user = User::where('email', $request->input('email'))->first();
		if( !empty($user) ){
			if( Hash::check($request->input('password'), $user->password) ){
				$api_token = str_random(60);
				User::where('email', $request->input('email'))->update(['api_token' => $api_token]);
				return response()->json(['status' => 'success', 'api_key' => $api_token]);
			}else{
				return response()->json(['status' => 'Unauthorize, Invalid Username/Password'], 401);
			}
		}else{
			return response()->json(['status' => 'Unauthorize, Invalid Username/Password'], 401);
		}
	}	
	
	public function view(Request $request, $id){
		if( $request->user()->role == 'admin' ){
			//$user = User::where('id', $id)->get();
			if( $request->user()->id != $id ){
				$user = Auth::user()->where('id', $id)->where('role', '!=', 'admin')->get();
			}else{
				$user = Auth::user()->where('id', $id)->get();
			}
		}else{
			if( $request->user()->id == $id ){
				$user = Auth::user()->where('id', $id)->get();
			}else{
				return response()->json(['error' => 'Unauthorize, You have no rights to view'], 401);	
			}
		}
		return response()->json($user, 200);
	}
	
	
	public function logout(Request $request){
	    $user = User::find($request->user_id);
		if( !empty($user) ){
			$update = $user->update(['api_token' => '']);
			if( $update ){
				return response()->json(['status' => 'logout successfully'], 201);
			}
		}
		return response()->json(['error' => 'Unauthorize'], 401);	
	}
	
}
