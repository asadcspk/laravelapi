<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Auth;

class BlogsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');	
	}
	
    public function index(Request $request){
		if( $request->user()->role == 'admin' ){
			$blog = Blog::all();
		}else{
			$blog = Auth::user()->blog()->get();
		}
        return response()->json(['status' => 'success','result' => $blog]);
	}
	
	public function store(Request $request){
		$this->validate($request, [
			'title'	=> 'required',
			'description'	=> 'required',
		]);
		
		if( Auth::user()->blog()->create($request->all()) ){
			return response()->json(['status' => 'success'], 201);
		}else{
            return response()->json(['status' => 'fail']);
        }
	}
	
	public function show(Request $request, $id){
		if( $request->user()->role == 'admin' ){
			$blog = Blog::where('id', $id)->get();
		}else{
			$blog = Auth::user()->blog()->where('id', $id)->get();
		}
		
        return response()->json($blog, 200);
	}
	
	public function edit($id)
    {
        $blog = Blog::where('id', $id)->get();
        return view('blog.editblog',['blog' => $blog]);
    }
	
	public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title'	=> 'required',
			'description'	=> 'required',
		]);
		
		$blog = Auth::user()->blog()->where('id', $id)->get()->first();
        //$blog = Blog::where(['id' => $id, 'user_id' => $request->input('user_id')])->first();
		//$blog = Blog::find($id);
		//echo "<pre>";print_r($blog);exit;
		if( !empty($blog) ){
			$blog->fill($request->all());
			if($blog->fill($request->all())->save()){
			   return response()->json(['status' => 'success'], 200);
			}
			return response()->json(['status' => 'failed'], 200);
		}
		return response()->json(['status' => 'Unauthorize, You have no right to update this blog'], 401);
    }
	
	public function destroy($id)
    {
        //if(Blog::destroy($id)){
        if(Auth::user()->blog()->where('id', $id)->delete()){
             return response()->json(['status' => 'success'], 200);
        }
    }
	
}
