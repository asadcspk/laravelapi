<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
	protected $table = "api_medias";
	
	protected $fillable = [ 'name', 'alt', 'url', 'user_id' ];
	
	protected $hidden = [
       'user_id'
    ];
	
}